#![no_std]
#![no_main]
#![feature(isa_attribute)]

use gba::prelude::*;

const BLACK: Color = Color::from_rgb(0, 0, 0);
const RED: Color = Color::from_rgb(31, 0, 0);
const GREEN: Color = Color::from_rgb(0, 31, 0);
const BLUE: Color = Color::from_rgb(0, 0, 31);
const YELLOW: Color = Color::from_rgb(31, 31, 0);
const PINK: Color = Color::from_rgb(31, 0, 31);

#[panic_handler]
#[allow(unused)]
fn panic(info: &core::panic::PanicInfo) -> ! {
    // This kills the emulation with a message if we're running inside an
    // emulator we support (mGBA or NO$GBA), or just crashes the game if we
    // aren't.
    //fatal!("{}", info);

    loop {
        DISPCNT.read();
    }
}

// Magic numbers found on https://www.coranac.com/tonc/text/sndsqr.htm
mod Notes {
    pub const C: u16 = 8013;
    pub const CS: u16 = 7566;
    pub const D: u16 = 7144;
    pub const DS: u16 = 6742;
    pub const E: u16 = 6362;
    pub const F: u16 = 6005;
    pub const FS: u16 = 5666;
    pub const G: u16 = 5346;
    pub const GS: u16 = 5048;
    pub const A: u16 = 4766;
    pub const AS: u16 = 4499;
    pub const B: u16 = 4246;
}

fn sound_rate(note: u16, oct: usize) -> u16 {
    return 2048 - ((note as u16) >> (4 + (oct)));
}

#[instruction_set(arm::a32)]
extern "C" fn irq_handler_a32() {
    // we just use this a32 function to jump over back to t32 code.
    irq_handler_t32()
}

fn irq_handler_t32() {
    // disable Interrupt Master Enable to prevent an interrupt during the handler
    unsafe { IME.write(false) };

    // read which interrupts are pending, and "filter" the selection by which are
    // supposed to be enabled.
    let which_interrupts_to_handle = IRQ_PENDING.read() & IE.read();

    // read the current IntrWait value. It sorta works like a running total, so
    // any interrupts we process we'll enable in this value, which we write back
    // at the end.
    let mut intr_wait_flags = INTR_WAIT_ACKNOWLEDGE.read();

    if which_interrupts_to_handle.vblank() {
        intr_wait_flags.set_vblank(true);
    }
    if which_interrupts_to_handle.timer1() {
        dma1_handler();
        intr_wait_flags.set_timer1(true);
    }
    if which_interrupts_to_handle.timer2() {
        dma1_handler();
        intr_wait_flags.set_timer2(true);
    }

    // acknowledge that we did stuff.
    IRQ_ACKNOWLEDGE.write(which_interrupts_to_handle);

    // write out any IntrWait changes.
    unsafe { INTR_WAIT_ACKNOWLEDGE.write(intr_wait_flags) };

    // re-enable as we go out.
    unsafe { IME.write(true) };
}

fn dma1_handler() {
    let freq_control = ToneFrequencyControl::new()
        .with_auto_stop(false)
        .with_frequency(sound_rate(Notes::C, 0))
        .with_restart(true);
    TONE1_FREQ_CNT.write(freq_control);
}

fn start_timers() {
    let init_val: u16 = u32::wrapping_sub(0xc000, 64) as u16;

    const TIMER_SETTINGS1: TimerControl = TimerControl::new()
        .with_irq_on_overflow(true)
        .with_enabled(true);

    const TIMER_SETTINGS2: TimerControl = TimerControl::new()
        .with_irq_on_overflow(true)
        .with_chained_counting(true)
        .with_enabled(true);

    TIMER1_RELOAD.write(init_val);
    TIMER1_CONTROL.write(TIMER_SETTINGS1.with_prescaler_selection(3));
    TIMER2_CONTROL.write(TIMER_SETTINGS2.with_prescaler_selection(1));
}

#[no_mangle]
pub fn main() -> ! {
    DISPCNT.write(
        DisplayControl::new()
            .with_display_mode(3)
            .with_display_bg2(true),
    );
    mode3::dma3_clear_to(BLACK);

    // Set the IRQ handler to use.
    unsafe { USER_IRQ_HANDLER.write(Some(irq_handler_a32)) };

    // Enable all interrupts that are set in the IE register.
    unsafe { IME.write(true) };

    let flags = InterruptFlags::new()
        .with_timer1(true)
        .with_timer2(true)
        .with_vblank(true);

    unsafe { IE.write(flags) };

    // Request that VBlank, HBlank and VCount will generate IRQs.
    const DISPLAY_SETTINGS: DisplayStatus = DisplayStatus::new().with_vblank_irq_enabled(true);
    DISPSTAT.write(DISPLAY_SETTINGS);

    let enable_sound = SoundStatus::new().with_enabled(true);
    SOUND_STATUS.write(enable_sound);

    let sound_control = SoundControl::new()
        .with_right_volume(7)
        .with_left_volume(7)
        .with_tone1_right(true)
        .with_tone1_left(true);
    SOUND_CONTROL.write(sound_control);

    let env_control = ToneDutyLenEnv::new().with_volume(15);
    TONE1_DUTY_LEN_ENV.write(env_control);

    let freq_control = ToneFrequencyControl::new()
        .with_auto_stop(false)
        .with_frequency(sound_rate(Notes::B, 0))
        .with_restart(true);
    TONE1_FREQ_CNT.write(freq_control);

    start_timers();

    loop {
        // The VBlank IRQ must be enabled at minimum, or else the CPU will halt
        // at the call to vblank_interrupt_wait() as the VBlank IRQ will never
        // be triggered.

        // Puts the CPU into low power mode until a VBlank IRQ is received. This
        // will yield considerably better power efficiency as opposed to spin
        // waiting.
        unsafe { VBlankIntrWait() };
    }
}
